﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noCoins : MonoBehaviour
{
    public Animation noCoinsAnimation;
    public void Play()
    {
        noCoinsAnimation.Play();
    }
}
