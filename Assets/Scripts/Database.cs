﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Database", menuName = "ScriptableObjects/Database", order = 2)]
public class Database : ScriptableObject
{
    public int coins;

    public int currentSkinIndex;

    public int currentFieldIndex;

    public int currentBallIndex;

    public int currentLevel;

    public List<SkinSO> skins = new List<SkinSO>();

    public List<BallSO> balls = new List<BallSO>();

    public List<FieldSO> fields = new List<FieldSO>();

    public List<LevelSO> levels = new List<LevelSO>();

    public void Initialize()
    {
        initializeDatabase();

        initializeSkins();

        initializeBalls();

        initializeFields();

        initializeLevels();
    }

    public void initializeDatabase()
    {
        if (!PlayerPrefs.HasKey("coins"))
        {
            PlayerPrefs.SetInt("coins", 100);
        }

        if (!PlayerPrefs.HasKey("currentSkin"))
        { 
            PlayerPrefs.SetString("currentSkin", skins[0].name);
        }

        if (!PlayerPrefs.HasKey("currentBall"))
        {
            PlayerPrefs.SetString("currentBall", balls[0].name);
        }

        if (!PlayerPrefs.HasKey("currentField"))
        {
            PlayerPrefs.SetString("currentField", fields[0].name);
        }

        if (!PlayerPrefs.HasKey("currentLevel"))
        {
           // PlayerPrefs.SetString("currentLevel", levels[0].name);
        }
    }

    public void initializeSkins()
    {
        if (!PlayerPrefs.HasKey("skinsInitialized"))
        {
            for (int i = 0; i < skins.Count; i++)
            {
                SkinSO skinSO = skins[i];

                if (i == 0)
                {
                    PlayerPrefs.SetString(skinSO.name, "unlocked");
                }
                else
                {
                    PlayerPrefs.SetString(skinSO.name, "locked");
                }
            }

            PlayerPrefs.SetString("skinsInitialized", "initialized");
        }
    }

    public void initializeBalls()
    {
        if (!PlayerPrefs.HasKey("ballsInitialized"))
        {
            for (int i = 0; i < balls.Count; i++)
            {
               BallSO ballSO = balls[i];

                if (i == 0)
                {
                    PlayerPrefs.SetString(ballSO.name, "unlocked");
                }
                else
                {
                    PlayerPrefs.SetString(ballSO.name, "locked");
                }
            }

            PlayerPrefs.SetString("ballsInitialized", "initialized");
        }
    }

    public void initializeFields()
    {
        if (!PlayerPrefs.HasKey("fieldsInitialized"))
        {
            for (int i = 0; i < fields.Count; i++)
            {
                FieldSO fieldSO = fields[i];

                if (i == 0)
                {
                    PlayerPrefs.SetString(fieldSO.name, "unlocked");
                }
                else
                {
                    PlayerPrefs.SetString(fieldSO.name, "locked");
                }
            }

            PlayerPrefs.SetString("fieldsInitialized", "initialized");
        }
    }

    public void initializeLevels()
    {
        if (!PlayerPrefs.HasKey("levelsInitialized"))
        {
            for (int i = 0; i < levels.Count; i++)
            {
                LevelSO levelSO = levels[i];

                if (i == 0)
                {
                    PlayerPrefs.SetString(levelSO.name, "unlocked");
                }

                PlayerPrefs.SetString(levelSO.name, "locked");
            }

            PlayerPrefs.SetString("levelsInitialized", "initialized");
        }
    }

    public int getCoins()
    {
        return PlayerPrefs.GetInt("coins");
    }

    public void IncreaseCoins(int numberOfCoins)
    {
        int currentCoins = PlayerPrefs.GetInt("coins");

        PlayerPrefs.SetInt("coins", currentCoins + numberOfCoins);
    }

    public void DecreaseCoins(int numberOfCoins)
    {
        int currentCoins = PlayerPrefs.GetInt("coins");

        PlayerPrefs.SetInt("coins", currentCoins - numberOfCoins);
    }

    public bool canBuy(int price)
    {
        int currentCoins = PlayerPrefs.GetInt("coins");

        if (currentCoins >= price)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void setCurrentSkin(string currentSkin)
    {
        PlayerPrefs.SetString("currentSkin", currentSkin);
    }

    public SkinSO getCurrentSkin()
    {
        return skins.Find(e => e.name == PlayerPrefs.GetString("currentSkin"));
    }

    public void setCurrentField(string currentField)
    {
        PlayerPrefs.SetString("currentField", currentField);
    }

    public FieldSO getCurrentField()
    {
        return fields.Find(e => e.name == PlayerPrefs.GetString("currentField"));
    }

    public void setCurrentBall(string currentBall)
    {
        PlayerPrefs.SetString("currentBall", currentBall);
    }

    public BallSO getCurrentBall()
    {
        return balls.Find(e => e.name == PlayerPrefs.GetString("currentBall"));
    }

    public void setCurrentLevel(string currentLevel)
    {
        PlayerPrefs.SetString("currentLevel", currentLevel);
    }

    public LevelSO getCurrentLevel()
    {
        return levels.Find(e => e.name == PlayerPrefs.GetString("currentLevel"));
    }

    public bool isUnlocked(string name)
    {
        string lockState = PlayerPrefs.GetString(name);

        if(lockState == "locked")
        {
            return false;
        }
        if(lockState == "unlocked")
        {
            return true;
        }

        Debug.Log("PROBLEM!!!");

        return false;
    }

    public void Unlock(string name)
    {
        PlayerPrefs.SetString(name, "unlocked");
    }
}
