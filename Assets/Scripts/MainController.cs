﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

public class MainController : MonoBehaviour
{
    public Database data;

    public SpriteRenderer fieldRenderer;
    public SpriteRenderer ballRenderer;
    public List<SpriteRenderer> playersRenderer;

    
    public TeamController hostTeam, guestTeam;

    public GameObject gameOver;

    public static MainController instance;

    public EndGameController endGameController;

    private List<Vector3> playerStartPos = new List<Vector3>();
    public List<GameObject> playersGameObjects;

    public LevelSO levelSo;
    public List<Animator> guestAnimators;

    void Awake()
    {
        fieldRenderer.sprite = data.getCurrentField().sprite;
        ballRenderer.sprite = data.getCurrentBall().sprite;

        for (int i = 0; i < playersRenderer.Count; i++)
        {
            playersRenderer[i].sprite = data.getCurrentSkin().sprite;
        }
        
        
        instance = this;

        for (int i=0; i< playersGameObjects.Count; i++)
        {
            playerStartPos.Add(playersGameObjects[i].transform.position);
        }

        Restart();
        AudioController.instance.Start();
    }

    public void GameOver(string mistake)
    {
        endGameController.OpenGameOver(mistake);
    }

    public void Restart()
    {
        for (int i = 0; i < playersGameObjects.Count; i++)
        {
            playersGameObjects[i].transform.position = playerStartPos[i];
        }
//
//
//
        for (int i = 0; i < levelSo.levels[levelSo.currentLevel].guests.Count; i++)
        {
            int index = levelSo.levels[levelSo.currentLevel].guests[i].guestIndex;
            playersGameObjects[index].SetActive(true);
            guestAnimators[index].SetLayerWeight(levelSo.levels[levelSo.currentLevel].guests[i].animationIndex, 1);
        }
    }
}
