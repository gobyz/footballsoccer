﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public Database database;

    public Image skinButton;
    public Image ballButton;
    public Image fieldButton;

    public GameObject skinMenu;
    public List<GameObject> skinOptions = new List<GameObject>();
    public List<Image> skinImages;
    public List<Skin> skins;
    public List<GameObject> skinsLocked = new List<GameObject>();
    public List<GameObject> skinsIndicators = new List<GameObject>();
    public List<Text> skinsPrices = new List<Text>();

    public GameObject levelMenu;
    public List<GameObject> levelOptions = new List<GameObject>();
    public List<Level> levles;

    public GameObject ballMenu;
    public List<GameObject> ballsOptions = new List<GameObject>();
    public List<Image> ballImages;
    public List<Ball> balls;
    public List<GameObject> ballsLocked = new List<GameObject>();
    public List<GameObject> ballsIndicators = new List<GameObject>();
    public List<Text> ballsPrices = new List<Text>();

    public GameObject fieldMenu;
    public List<GameObject> fieldOptions = new List<GameObject>();
    public List<Image> fieldImages;
    public List<Field> fields;
    public List<GameObject> fieldLocked = new List<GameObject>();
    public List<GameObject> fieldIndicators = new List<GameObject>();
    public List<Text> fieldPrices = new List<Text>();

    public GameObject coinsObject;
    public Text coinsText;
    public Animation noCoinsAnimation;

    public void showCoins()
    {
        coinsObject.SetActive(true);
    }

    public void hideCoins()
    {
        coinsObject.SetActive(false);
    }

    void Start()
    {
        SetCurrentSkinImage();

        SetCurrentBallImage();

        SetCurrentFieldImage();
    }

    public void SetCurrentSkinImage()
    {
        skinButton.sprite = database.getCurrentSkin().sprite;
    }

    public void SetCurrentBallImage()
    {
        ballButton.sprite = database.getCurrentBall().sprite;
    }

    public void SetCurrentFieldImage()
    {
        fieldButton.sprite = database.getCurrentField().buttonSprite;
    }

    public void RefreshSkins()
    {
        foreach(GameObject lockObject in skinsLocked)
        {
            lockObject.SetActive(false);
        }

        foreach (GameObject indicator in skinsIndicators)
        {
            indicator.SetActive(false);
        }

        for (int i = 0; i < skins.Count; i++)
        {
            if (skins[i].isLocked())
            {
                skinsLocked[i].SetActive(true);
            }
            else
            {
                skinsLocked[i].SetActive(false);
            }
        }

        for (int i = 0; i < skins.Count; i++)
        {
            if (skins[i].skinSO == database.getCurrentSkin())
            {
                skinsIndicators[i].SetActive(true);
            }
        }

        for (int i = 0; i < skins.Count; i++)
        {
                skinsPrices[i].text = skins[i].skinSO.price.ToString();
        }

        SetCurrentSkinImage();

        coinsText.text = database.getCoins().ToString();
    }

    public void RefreshBalls()
    {
        foreach (GameObject lockObject in ballsLocked)
        {
            lockObject.SetActive(false);
        }

        foreach (GameObject indicator in ballsIndicators)
        {
            indicator.SetActive(false);
        }

        for (int i = 0; i < balls.Count; i++)
        {
            if (balls[i].isLocked())
            {
                ballsLocked[i].SetActive(true);
            }
            else
            {
                ballsLocked[i].SetActive(false);
            }
        }

        for (int i = 0; i < balls.Count; i++)
        {
            if (balls[i].ballSO == database.getCurrentBall())
            {
                ballsIndicators[i].SetActive(true);
            }
        }

        for (int i = 0; i < balls.Count; i++)
        {
            ballsPrices[i].text = balls[i].ballSO.price.ToString();
        }

        SetCurrentBallImage();

        coinsText.text = database.getCoins().ToString();
    }
    public void RefreshFields()
    {
        foreach (GameObject lockObject in fieldLocked)
        {
            lockObject.SetActive(false);
        }

        foreach (GameObject indicator in fieldIndicators)
        {
            indicator.SetActive(false);
        }

        for (int i = 0; i < fields.Count; i++)
        {
            if (fields[i].isLocked())
            {
                fieldLocked[i].SetActive(true);
            }
            else
            {
                fieldLocked[i].SetActive(false);
            }
        }

        for (int i = 0; i < fields.Count; i++)
        {
            if (fields[i].fieldSO == database.getCurrentField())
            {
                fieldIndicators[i].SetActive(true);
            }
        }

        for (int i = 0; i < fields.Count; i++)
        {
            fieldPrices[i].text = fields[i].fieldSO.price.ToString();
        }

        SetCurrentFieldImage();

        coinsText.text = database.getCoins().ToString();
    }

    public void OpenShop()
    {

    }

    public void Back()
    {

    }

    public void BuyItem()
    {

    }

    public void OpenBallMenu()
    {
        for (int i=0; i<database.balls.Count; i++)
        {
            ballsOptions[i].SetActive(true);
            balls[i].ballSO = database.balls[i];
            ballImages[i].sprite = balls[i].ballSO.sprite;
        }

        ballMenu.SetActive(true);

        showCoins();
    }

    public void OpenFieldMenu()
    {
        for (int i = 0; i < database.fields.Count; i++)
        {
            fieldOptions[i].SetActive(true);
            fields[i].fieldSO = database.fields[i];
            fieldImages[i].sprite = fields[i].fieldSO.buttonSprite;
        }

        fieldMenu.SetActive(true);

        showCoins();
    }

    public void OpenSkinMenu()
    {
        for (int i = 0; i < database.skins.Count; i++)
        {
            skinOptions[i].SetActive(true);
            skins[i].skinSO = database.skins[i];
            skinImages[i].sprite = skins[i].skinSO.sprite;
        }

        skinMenu.SetActive(true);

        showCoins();
    }

    public void CloseSkinMenu()
    {
        skinMenu.SetActive(false);

        hideCoins();
    }

    public void CloseBallMenu()
    {
        ballMenu.SetActive(false);

        hideCoins();

    }

    public void CloseFieldMenu()
    {
        fieldMenu.SetActive(false);

        hideCoins();
    }
}
