﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

public class GateHelper : MonoBehaviour
{

    public static GateHelper instance;

    void Awake()
    {
        instance = this;
    }
    
    private bool enter, leave;
    public void ResetGate()
    {
        enter = false;
        leave = false;
    }

    public bool GatePassed()
    {
        return enter && leave;
    }


    public void Enter()
    {
        enter = true;
    }

    public void Leave()
    {
        leave = true;
    }
    
    
//    private void OnTriggerEnter(Collider other)
//    {
//        if (other.tag.Equals("Player") && other.gameObject.GetComponent<Player>().role == 0)
//        {
//            Debug.Log("OnTriggerEnter");
//            enter = true;
//        }
//    }
//    
//    private void OnTriggerExit(Collider other)
//    {
//        if (other.tag.Equals("Player") && other.gameObject.GetComponent<Player>().role == 0)
//        {
//            Debug.Log("OnTriggerExit");
//            leave = true;
//        }
//    }
}
