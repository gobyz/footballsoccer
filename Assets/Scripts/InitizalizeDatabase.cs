﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitizalizeDatabase : MonoBehaviour
{
    public Database database;
    // Start is called before the first frame update
    
    void Start()
    {
        if (!PlayerPrefs.HasKey("Database"))
        {
            database.Initialize();

            PlayerPrefs.SetString("Database", "initialized");
        }
    }
}
