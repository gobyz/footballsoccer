﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	public TeamController teamController;
	public AnimationCurve runningCurve;
	public GameObject touchColliderGO;
	public GameObject arrow;

	

	public GameObject ballSlot;
	public GameObject ballRotationHelper;
	public GameObject ball;
	public BallController ballController;

	private Vector3 startPosition;
	private Vector3 targetPosition;
	private float runningTime;
	private bool isRunning;


	public int role;

	public bool inGoalZone;

	public Vector3 force;
	
	public void Move(Vector3 toPosition)
	{
		if (!isRunning)
		{
			startPosition = transform.position;
			targetPosition = toPosition;
			StartCoroutine(Moving());
		}
	}


	IEnumerator Moving()
	{
		isRunning = true;
		runningTime = 0;
		touchColliderGO.SetActive(false);
		
		while (runningTime < 1)
		{
			transform.position = Vector3.Lerp(startPosition, targetPosition, runningCurve.Evaluate(runningTime));
			runningTime += Time.deltaTime;
			yield return null;
		}

		transform.position = targetPosition;
		
		
		ballSlot.transform.rotation = Quaternion.identity;
		ball.transform.parent = ballRotationHelper.transform;
		ball.transform.localPosition = Vector3.zero;

		if (!GateHelper.instance.GatePassed())
		{
			AudioController.instance.GoalOut();
			MainController.instance.GameOver("FAIL ACTION!");
		}
		else if (inGoalZone)
		{
			ballController.Shot(force);
		}
		
		teamController.SetPlayerWithBall(this);
		teamController.TurnOnTouchColliders();
		isRunning = false;
		
	}
	
	
	float RemapValueRange(float value, float rangeFromMin, float rangeFromMax, float rangeToMin, float rangeToMax)
	{
		return rangeToMin + (value-rangeFromMin)*(rangeToMax-rangeToMin)/(rangeFromMax-rangeFromMin);
	}
	
	private void OnTriggerEnter(Collider other)
	{
		if (isRunning)
		{
			if (other.tag.Equals("Enemy"))
			{
				AudioController.instance.Fault();
				MainController.instance.GameOver("FAULT!");
			}
			if (other.tag.Equals("Player"))
			{
				AudioController.instance.Fault();
				MainController.instance.GameOver("TEAMMATE FAULT!");
			}
			else if (other.tag.Equals("Gate") && role == 0)
			{
				GateHelper.instance.Enter();
			}
			else if (other.tag.Equals("GoalZone"))
			{
				inGoalZone = true;
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (isRunning)
		{
			if (other.tag.Equals("Gate") && role == 0)
			{
				GateHelper.instance.Leave();
			}
			else if (other.tag.Equals("GoalZone"))
			{
				inGoalZone = false;
			}
		}
	}
}
