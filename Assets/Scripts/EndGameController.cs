﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGameController : MonoBehaviour
{
    public MainController mainController;
    public GameObject gameOver;
    public Text gameOverText;

    public GameObject winningScreen;
    public List<GameObject> starsImages; 

    public GameObject pauseScreen;

    public LevelSO levelSo;

    public void OpenGameOver(string text)
    {
        gameOver.SetActive(true);
        gameOverText.text = text;
    }

    public void LoadMenuScene()
    {
        SceneManager.LoadScene("MenuScene");
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Main");
       // mainController.Restart();
    }

    public void OpenWinningScreen(int totalMoves)
    {
        int starsCount = 3;

        if (totalMoves > levelSo.levels[levelSo.currentLevel].threeStarMoves)
            starsCount--;
        if (totalMoves > levelSo.levels[levelSo.currentLevel].twoStarMoves)
            starsCount--;
        if (totalMoves > levelSo.levels[levelSo.currentLevel].oneStarMoves)
            starsCount--;

        for (int i=0; i<starsImages.Count; i++)
        {
            if (i<starsCount)
            {
                starsImages[i].SetActive(true);
            }
            else
            {
                starsImages[i].SetActive(false);
            }
        }

        
        
        levelSo.currentLevel++;

        if (levelSo.currentLevel >= levelSo.levels.Count)
        {
            levelSo.currentLevel = levelSo.levels.Count - 1;
        }

        winningScreen.SetActive(true);
    }

    public void OpenPauseScreen()
    {
        pauseScreen.SetActive(true);
    }

    public void ClosePauseScreen()
    {
        pauseScreen.SetActive(false);
    }
}
