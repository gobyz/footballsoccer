﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;
using System.Linq;
using System.Numerics;

public class TeamController : MonoBehaviour
{
    public GameObject angleIndicator;
    public GateHelper gateHelper;
    public List<Player> players;
    private Vector2 mouseButtonDownPos;

    public BallController ballController;

    public GameObject gate;

    private Camera mainCamera;

    public Player controllingPlayer;
    public Player playerWithTheBall;
    private bool playerSelected;


    private Vector3 aimingPosition;
    private float power;

    private bool playerMoving;


    public int tries;

    
    private void Start()
    {
        mainCamera = Camera.main;;
    }


    private void Update()
    {
        UserInput();
    }


    public void TurnOffTouchColliders()
    {
        for (int i = 0; i < players.Count; i++)
        {
            players[i].touchColliderGO.SetActive(false);
        }
    }
    public void TurnOnTouchColliders()
    {
        for (int i = 0; i < players.Count; i++)
        {
            players[i].touchColliderGO.SetActive(true);
        }
    }
    
    

    public void UserInput()
    {
        if (Input.GetMouseButtonDown(0) && (Application.isEditor && !EventSystem.current.IsPointerOverGameObject() ||
                              !Application.isEditor &&
                              !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)))
            
            
        {

            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                controllingPlayer = players.Find(x => x.gameObject == hit.transform.gameObject);

                if (controllingPlayer != null && controllingPlayer.role != 1)
                {
                    TurnOffTouchColliders();
                    for (int i = 0; i < players.Count; i++)
                    {
                        players[i].role = 2;
                    }
                    playerWithTheBall.role = 1;
                    
                    controllingPlayer.role = 0;
                    mouseButtonDownPos = Input.mousePosition;
                    controllingPlayer.arrow.SetActive(true);
                    playerSelected = true;
                    
                    //ANGLE

                    Vector3 middlePoint = (players.Find(x => x.role == 1).transform.position +
                                           players.Find(x => x.role == 2).transform.position) / 2;
                    
                    angleIndicator.SetActive(true);
                    angleIndicator.transform.position = controllingPlayer.transform.position;
                    
                    angleIndicator.transform.LookAt(middlePoint);


                    float distance = Vector3.Distance(middlePoint, controllingPlayer.transform.position);
                    
                    angleIndicator.transform.localScale = new Vector3(distance,distance,distance);
                }
                else
                {
                    angleIndicator.SetActive(false);
                    playerSelected = false;
                }
            }
        }

        if (Input.GetMouseButtonUp(0) && playerSelected)
        {
            AudioController.instance.Pass();
            tries++;
            angleIndicator.SetActive(false);
            controllingPlayer.arrow.SetActive(false);
            playerSelected = false;

            Vector3 force = controllingPlayer.transform.position - aimingPosition;
            force = new Vector3(force.x, 0, force.z);
            
            
            Vector3 target = controllingPlayer.transform.position + force.normalized * power;

            controllingPlayer.force = force;
            controllingPlayer.Move(target);
            ballController.Pass(target);
            playerWithTheBall = controllingPlayer;
            

            Vector3 role1Pos = players.Find(x => x.role == 1).transform.position;
            Vector3 role2Pos = players.Find(x => x.role == 2).transform.position;
            
            
            float gateSize = Vector3.Distance(
                role1Pos,
                role2Pos);
            
            gate.transform.localScale = new Vector3(gateSize,0.1f,0.1f);
            gate.transform.position = (role1Pos + role2Pos) / 2;

            gate.transform.LookAt(role1Pos);
            gate.transform.Rotate(0,90,0);
            gateHelper.ResetGate();

           
        }

        if (playerSelected)
        {
            aimingPosition = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, mainCamera.transform.position.y));
            controllingPlayer.arrow.transform.LookAt(aimingPosition);

            power = Vector3.Distance(aimingPosition, controllingPlayer.transform.position);
            power = RemapValueRange(power, 0, 0.5f, 0, 1);
            power = Mathf.Clamp(power, 0, 1);

            float scale = power;
            controllingPlayer.arrow.transform.localScale = new Vector3(scale, scale, scale*2);
 
        } 
    }


    public void SetPlayerWithBall(Player player)
    {
        for (int i = 0; i < players.Count; i++)
        {
            players[i].role = 2;
        }

        players.Find(x => x == player).role = 1;
    }
    
    
    
    
    float RemapValueRange(float value, float rangeFromMin, float rangeFromMax, float rangeToMin, float rangeToMax)
    {
        return rangeToMin + (value-rangeFromMin)*(rangeToMax-rangeToMin)/(rangeFromMax-rangeFromMin);
    }
}
