﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Ball : MonoBehaviour
{
    public Database database;

    public BallSO ballSO;

    public UnityEvent noCoins;
    public void buyBall()
    {
        if (database.canBuy(ballSO.price) && !database.isUnlocked(ballSO.name))
        {
            database.DecreaseCoins(ballSO.price);

            database.Unlock(ballSO.name);

            database.setCurrentBall(ballSO.name);
        }
        if (!database.canBuy(ballSO.price))
        {
            noCoins.Invoke();
        }

        if (database.isUnlocked(ballSO.name))
        {
            database.setCurrentBall(ballSO.name);
        }
    }

    public bool isLocked()
    {
        return !database.isUnlocked(ballSO.name);
    }
}
