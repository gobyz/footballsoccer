﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BallSO", menuName = "ScriptableObjects/BallSO", order = 4)]
public class BallSO : ScriptableObject
{
    public Sprite sprite;

    public int price;
}
