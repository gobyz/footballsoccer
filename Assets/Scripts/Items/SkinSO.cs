﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkinSO", menuName = "ScriptableObjects/SkinSO", order = 1)]
public class SkinSO : ScriptableObject
{
    public Sprite sprite;

    public int price;
}
