﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Field : MonoBehaviour
{
    public Database database;

    public FieldSO fieldSO;

    public UnityEvent noCoins;

    public void buyField()
    {
        if (database.canBuy(fieldSO.price) && !database.isUnlocked(fieldSO.name))
        {
            database.DecreaseCoins(fieldSO.price);

            database.Unlock(fieldSO.name);

            database.setCurrentField(fieldSO.name);
        }
        if (!database.canBuy(fieldSO.price))
        {
            noCoins.Invoke();
        }

        if (database.isUnlocked(fieldSO.name))
        {
            database.setCurrentField(fieldSO.name);
        }
    }

    public bool isLocked()
    {
        return !database.isUnlocked(fieldSO.name);
    }
}
