﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelSO", menuName = "ScriptableObjects/LevelSO", order = 3)]
public class LevelSO : ScriptableObject
{
    public int currentLevel;
    public List<LevelInfo> levels;
}

[Serializable]
public class LevelInfo
{
    public List<Guest> guests;
    public int threeStarMoves;
    public int twoStarMoves;
    public int oneStarMoves;
}

[Serializable]
public class Guest
{
    public int guestIndex;
    public int animationIndex;
}
