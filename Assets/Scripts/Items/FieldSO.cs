﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FieldSO", menuName = "ScriptableObjects/FieldSO", order = 5)]
public class FieldSO : ScriptableObject
{
    public Sprite sprite;

    public Sprite buttonSprite;

    public int price;
}
