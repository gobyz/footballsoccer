﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public Database database;

    public LevelSO levelSO;
    public void unlockLevel()
    {
        if (!database.isUnlocked(levelSO.name))
        { 
            database.Unlock(levelSO.name);
        }
    }

    public bool isLocked()
    {
        return !database.isUnlocked(levelSO.name);
    }
}
