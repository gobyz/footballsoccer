﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Skin : MonoBehaviour
{
    public Database database;

    public SkinSO skinSO;

    public UnityEvent noCoins;

    public void buySkin()
    {
        if (database.canBuy(skinSO.price) && !database.isUnlocked(skinSO.name))
        {
            database.DecreaseCoins(skinSO.price);

            database.Unlock(skinSO.name);

            database.setCurrentSkin(skinSO.name);
        }
        if(!database.canBuy(skinSO.price))
        {
            noCoins.Invoke();
        }

        if (database.isUnlocked(skinSO.name))
        {
            database.setCurrentSkin(skinSO.name);
        }
    }

    public bool isLocked()
    {
        return !database.isUnlocked(skinSO.name);
    }
}
