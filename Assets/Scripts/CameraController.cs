﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class CameraController : MonoBehaviour
{
    public GameObject ball;

    public Camera mainCamera;


    public float cameraClampX, cameraClampZ;

    private Vector3 targetLocation;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        targetLocation = new Vector3(Mathf.Clamp(ball.transform.position.x, -cameraClampX,cameraClampX),mainCamera.transform.position.y,
            Mathf.Clamp(ball.transform.position.z, -cameraClampX,cameraClampZ)
            );
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, targetLocation,Time.deltaTime);
        
        
    }
}
