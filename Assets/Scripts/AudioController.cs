﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController instance;

    public AudioSource start;
    public AudioSource goal;
    public AudioSource fault;
    public AudioSource kick;
    public AudioSource goalOut;
    public AudioSource pass;

    void Awake()
    {
        instance = this;
    }

    public void Start()
    {
        start.Play();
    }

    public void Goal()
    {
        goal.Play();
    }


    public void Fault()
    {
        fault.Play();
    }

    public void Kick()
    {
        kick.Play();
    }

    public void GoalOut()
    {
        goalOut.Play();
    }

    public void Pass()
    {
        pass.Play();
    }
}
