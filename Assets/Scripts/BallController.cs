﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class BallController : MonoBehaviour
{
    public AnimationCurve curve;

    private bool passing;

    private Vector3 startPosition;
    private Vector3 endPosition;
    private float transitionTime;
    private float transitionEnd;

    private bool goal;

    public void Pass(Vector3 pos)
    {
        startPosition = transform.position;
        endPosition = pos;
       
        transitionTime = 0;
        transitionEnd = 0.9f;
        passing = true;
        StartCoroutine(Moving());

    }


    public void Shot(Vector3 pos)
    {
        AudioController.instance.Kick();
        startPosition = transform.position;
        endPosition = startPosition + pos;
       
        transitionTime = 0;
        transitionEnd = 0.9f;
        passing = true;
        StartCoroutine(Moving());
    }

    
    IEnumerator Moving()
    {
        while (transitionTime < 0.9f)
        {
            transform.position = Vector3.Lerp(startPosition, endPosition, curve.Evaluate(transitionTime/0.9f));
            transitionTime += Time.deltaTime;
            yield return null;
//            if (goal)
//            {
//                break;
//            }
        }

        if (!goal)
        {
            transform.position = endPosition;
        }
        
        passing = false;
    }
    
    public void Delay(Action action, float delay)
    {
        StartCoroutine(DelayC(action,delay));
    }


    IEnumerator DelayC(Action action, float delay)
    {
        yield return new WaitForSeconds(delay);
        action();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (passing && !goal)
        {
            if (other.tag.Equals("Enemy"))
            {
                AudioController.instance.Goal();
                MainController.instance.GameOver("BALL LOST!");
            }
            else if (other.tag.Equals("Goal"))
            {
                AudioController.instance.Goal();
                MainController.instance.endGameController.OpenWinningScreen(MainController.instance.hostTeam.tries);
                goal = true;
            }
            else if (other.tag.Equals("Out"))
            {
                AudioController.instance.GoalOut();
                MainController.instance.GameOver("OUT!");
            }
        }
    }

   
}
